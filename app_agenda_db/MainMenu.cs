﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace app_agenda_db
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
            this.comboBox1.Items.AddRange(Queries.db.UTENTEs.OrderBy(ut => ut.id).Select(u => u.nome).ToArray());
        }

        private void AdminModeButton_CLick(object sender, EventArgs e)
        {
            //this.Hide();
            AdminMenu adminMenu = new AdminMenu();
            adminMenu.ShowDialog();
        }

        private void UserModeButtonCLick(object sender, EventArgs e)
        {
            //this.Hide();
            UserMenu userMenu = new UserMenu(this.comboBox1.SelectedIndex + 1);
            userMenu.ShowDialog();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            if (this.textBox1.TextLength > 0)
            {
                Queries.AggiungiUtente(this.textBox1.Text);
                this.textBox1.ResetText();
                this.comboBox1.SelectedIndex = -1;
                this.comboBox1.Items.Clear();
                this.comboBox1.Items.AddRange(Queries.db.UTENTEs.OrderBy(ut => ut.id).Select(u => u.nome).ToArray());
            }
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.button2.Enabled = true;
        }
    }
}
