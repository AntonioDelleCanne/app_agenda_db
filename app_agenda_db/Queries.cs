﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app_agenda_db
{
    public static class Queries
    {
        public static AgendaDataClassesDataContext db = new AgendaDataClassesDataContext();
        public static readonly string VALUTA_EURO = "euro";
        public static readonly string VALUTA_PV = "pv";
        public static readonly string VALUTA_PP = "pp";
        public static readonly string TIPO_SPESA = "spesa";
        public static readonly string TIPO_PROMEMORIA = "promemoria"; //ha solo data inizio
        public static readonly string TIPO_EVENTO = "evento"; //ha solo data fine
        public static readonly int ID_ADMIN = 1;


        //restituisce le attività relative all'utente passato
        public static IQueryable<ATTIVITÀ> GetAttivitaUtente(int idUtente) //TODO aggiungi visionati
        {
            return from ua in db.U_ATTIVITAs
                   join a in db.ATTIVITÀs on ua.attività equals a.id
                   where ua.utente == idUtente
                   select a;
        }

        public static ATTIVITÀ GetAttivitaUtentePerNome(int idUtente, string nomeAttivita)
        {
            return GetAttivitaUtente(idUtente).Where(a => a.nome == nomeAttivita).FirstOrDefault();
        }

        public static PENITENZA GetPenitenzaAttivitaUtentePerNome(int idUtente, string nomeAttivita)
        {
            return (from i in db.IMPLICAZIONEs
                    join p in db.PENITENZAs on i.penitenza equals p.id
                    where i.attività == GetAttivitaUtentePerNome(idUtente, nomeAttivita).id
                    select p).FirstOrDefault();
        }

        private static int getNextIdAttività()
        {
            List<ATTIVITÀ> attSorted = db.ATTIVITÀs.OrderBy(a => a.id).ToList();
            for (int i = 1; i <= attSorted.Count(); i++)
            {
                if (attSorted.ElementAt(i - 1).id != i)
                    return i;
            }
            return attSorted.Count() + 1;
        }

        private static int getNextIdAttivitàOra(int idUtente, DateTime giorno)
        {
            List<ATTIVITÀ_ORA> attSorted = db.ATTIVITÀ_ORAs.Where(a => a.utente == idUtente && (a.giorno.Year > giorno.Year ? 1 : a.giorno.Year < giorno.Year ? -1 : a.giorno.Month > giorno.Month ? 1 : a.giorno.Month < giorno.Month ? -1 : a.giorno.Day > giorno.Day ? 1 : a.giorno.Day < giorno.Day ? -1 : 0) == 0).OrderBy(a => a.id).ToList();
            for (int i = 1; i <= attSorted.Count(); i++)
            {
                if (attSorted.ElementAt(i - 1).id != i)
                    return i;
            }
            return attSorted.Count() + 1;
        }

        public static void SetPenitenzaAttivitaUtentePerNome(int idUtente, PENITENZA penitenza)
        {

        }

        public static void SetBudgetUtente(int userId, int value, DateTime dataInizio)
        {
            string query = "begin tran addAttività;";
            var budgetDb = db.BUDGETs.Where(b => b.utente == userId && b.valuta == VALUTA_EURO && (b.data_inizio.Year > dataInizio.Year ? 1 : b.data_inizio.Year < dataInizio.Year ? -1 : b.data_inizio.Month > dataInizio.Month ? 1 : b.data_inizio.Month < dataInizio.Month ? -1 : b.data_inizio.Day > dataInizio.Day ? 1 : b.data_inizio.Day < dataInizio.Day ? -1 : 0) == 0);
            if (budgetDb.Any()) // modify
            {
                query += "update BUDGET set budget = {1} where utente = {2} and valuta = {3};";

            }else //add
            {
                query += "insert into BUDGET(data_inizio,budget,utente,valuta) values({0},{1},{2},{3});";
            }
            query += "commit tran addAttività;";
            try
            {
                db.ExecuteCommand(query, dataInizio.ToString(), value.ToString(), userId.ToString(), VALUTA_EURO);
                //continua solo se la query precedente è andata buon fine
                db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.BUDGETs);
                db = new AgendaDataClassesDataContext();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        //aggiunge o modifica se già esiste l'attività passata con la relativa categoria al relativo utente
        public static void SetAttivitaUtente(int idUtente, string nomeOriginale, string nome, int colore, string descrizione, string tipologia, Dictionary<string,int> valutaCosto, Dictionary<string, int> valutaProfitto, int idPenitenza) 
        {
            ATTIVITÀ nuovaAttivita = new ATTIVITÀ
            {
                id = getNextIdAttività(),
                nome = nome,
                colore = colore,
                descrizione = descrizione
            };
            string query = "begin tran addAttività;" +
                "alter table ATTIVITÀ nocheck constraint IDATTIVITÀ_CHK;";

            if (!(nomeOriginale is null))
            {
                var attDb = GetAttivitaUtente(idUtente).Where(e => e.nome == nomeOriginale);
                if (attDb.Any()) // modify
                {
                    nuovaAttivita.id = attDb.First().id;
                    query += "update ATTIVITÀ set nome = {1}, descrizione = {2}, colore = {3} where id = {0};";
                    query += "update CARATTERIZZAZIONE set tipologia = {5} where attività = {0};";
                    //query += "delete from COSTO where attività = {0};";
                    //query += "delete from PROFITTO where attività = {0};";
                    var penitDb = db.IMPLICAZIONEs.Where(t => t.attività == attDb.First().id);
                    if (idPenitenza >= 0)
                    {
                        if (penitDb.Any())
                            query += "update IMPLICAZIONE set penitenza = {6} where attività = {0};";
                        else
                            query += "insert into IMPLICAZIONE(attività, penitenza) values({0},{6});";
                    }

                }
            }
            else //add
            {
                query += "insert into ATTIVITÀ(id, nome, descrizione, colore) values({0},{1},{2},{3});" +
                    "insert into U_ATTIVITA(attività, utente) values({0},{4});";
                //aggiunta tipologia
                query += "insert into CARATTERIZZAZIONE(attività, tipologia) values({0}, {5});";
                //aggiunta penitenza
                if (idPenitenza >= 0)
                    query += "insert into IMPLICAZIONE(attività, penitenza) values({0},{6});";
                //Aggiunta costo

            }
            //uncoment sotto per far avere pù tipologie all'attività
            /*if ((from ta in db.CARATTERIZZAZIONEs where (ta.attività == nuovaAttivita.id && ta.tipologia == tipologia) select ta).Any())
                query += "update CARATTERIZZAZIONE set tipologia = {5} where attività = {0};";
            else
                query += "insert into CARATTERIZZAZIONE(attività, tipologia) values({0}, {5});";*/ 
            query += "alter table ATTIVITÀ with check check constraint ALL;" +
                "commit tran addAttività;";
            try
            {
                db.ExecuteCommand(query, nuovaAttivita.id.ToString(), nuovaAttivita.nome, nuovaAttivita.descrizione, nuovaAttivita.colore, idUtente.ToString(), tipologia, idPenitenza);
                //continua solo se la query precedente è andata buon fine
                //rimozione costo e profitto precedenti se presenti
                db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.ATTIVITÀs);
                db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.U_ATTIVITAs);
                db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.CARATTERIZZAZIONEs);
                db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.COSTOs);
                db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.PROFITTOs);
                db = new AgendaDataClassesDataContext(); //TODO trova modo per non riinizializzare ogni volta
                var toDeleteC = from e in db.COSTOs
                where (e.attività == nuovaAttivita.id)
                select e;
                foreach(var el in toDeleteC)
                {
                    db.COSTOs.DeleteOnSubmit(el);
                }
                var toDeleteP = from e in db.PROFITTOs
                               where (e.attività == nuovaAttivita.id)
                               select e;
                foreach (var el in toDeleteP)
                {
                    db.PROFITTOs.DeleteOnSubmit(el);
                }
                db.SubmitChanges();
                db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.COSTOs);
                db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.PROFITTOs);
                db = new AgendaDataClassesDataContext();
                //aggiunta costo e profitto
                foreach (KeyValuePair<string, int> entry in valutaCosto)
                {
                    COSTO nuovoCosto = new COSTO
                    {
                        attività = nuovaAttivita.id,
                        valuta = entry.Key,
                        quantità = entry.Value
                    };
                    db.COSTOs.InsertOnSubmit(nuovoCosto);
                }
                foreach (KeyValuePair<string, int> entry in valutaProfitto)
                {
                    PROFITTO nuovoProfitto = new PROFITTO
                    {
                        attività = nuovaAttivita.id,
                        valuta = entry.Key,
                        quantità = entry.Value
                    };
                    db.PROFITTOs.InsertOnSubmit(nuovoProfitto);
                }
                db.SubmitChanges();
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.ATTIVITÀs);
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.U_ATTIVITAs);
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.CARATTERIZZAZIONEs);
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.COSTOs);
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.PROFITTOs);
            db = new AgendaDataClassesDataContext(); //TODO trova modo per non riinizializzare ogni volta
        }

        public static void SetAttivitaOraUtente(int idUtente, string nomeAttivita, DateTime giorno,TimeSpan[] times)
        {
            int idAttivita = Decimal.ToInt32(GetAttivitaUtentePerNome(idUtente, nomeAttivita).id);
            //aggiungere giornata calendario se assente
            var giornata = db.GIORNATA_CALENDARIOs.Where(a => (a.giorno.Year > giorno.Year ? 1 : a.giorno.Year < giorno.Year ? -1 : a.giorno.Month > giorno.Month ? 1 : a.giorno.Month < giorno.Month ? -1 : a.giorno.Day > giorno.Day ? 1 : a.giorno.Day < giorno.Day ? -1 : 0) == 0);
            if (giornata.Count() < 1)
            {
                db.GIORNATA_CALENDARIOs.InsertOnSubmit(new GIORNATA_CALENDARIO
                {
                    giorno = giorno,
                    utente = idUtente,
                    penitenza_applicata = false
                });
                db.SubmitChanges();
                db = new AgendaDataClassesDataContext();
            }


            var toInsert = new ATTIVITÀ_ORA
            {
                attività = idAttivita,
                utente = idUtente,
                giorno = giorno,
                id = getNextIdAttivitàOra(idUtente, giorno),
                completamento = false,
                ora_inizio = times[0]
            };
            if (times.Length > 1)
            {
                toInsert.ora_fine = times[1];
                toInsert.tipo = TIPO_EVENTO;
            }
            else
                toInsert.tipo = TIPO_PROMEMORIA;

            db.ATTIVITÀ_ORAs.InsertOnSubmit(toInsert);
            db.SubmitChanges();
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.GIORNATA_CALENDARIOs);
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.ATTIVITÀ_ORAs);
            db = new AgendaDataClassesDataContext();
        }

        public static void EliminaAttivitaUtentePerNome(int idUtente, string nomeAttivita )
        {
            ATTIVITÀ toDelete = GetAttivitaUtentePerNome(idUtente, nomeAttivita);
            db.U_ATTIVITAs.DeleteAllOnSubmit(db.U_ATTIVITAs.Where(t => t.attività == toDelete.id && t.utente == idUtente));
            db.SubmitChanges();
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.U_ATTIVITAs);
        }

        /*public static void EliminaAttivita(int idAttivita)// la usa l'admin
        {
            //Cancellazione profitti
            var profitti = db.PROFITTOs.Where(p => p.attività == toDelete.id);
            db.PROFITTOs.DeleteAllOnSubmit(profitti);
            //Cancellazione  costi
            var costi = db.COSTOs.Where(c => c.attività == toDelete.id);
            db.COSTOs.DeleteAllOnSubmit(costi);
            db.SubmitChanges();
            //Cancellazione attività e tabelle collegate
            string query = "begin tran addAttività;" +
                "alter table ATTIVITÀ nocheck constraint IDATTIVITÀ_CHK;" +
                "alter table ATTIVITÀ nocheck constraint U_IDATTIVITÀ_CHK;";

            //query += "delete from U_ATTIVITA where attività = {0};";
            query += "delete from IMPLICAZIONE where attività = {0};";
            query += "alter table ATTIVITÀ with check check constraint ALL;" +
                "commit tran addAttività;";
            db.ExecuteCommand(query, toDelete.id, idUtente);

            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.ATTIVITÀs);
        }*/

        public static void AddTipologia(string nuovoNome)
        {
            var nuovo = new TIPOLOGIA { nome = nuovoNome };
            if (!db.TIPOLOGIAs.Contains(nuovo))
                db.TIPOLOGIAs.InsertOnSubmit(nuovo);
            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static IQueryable<ATTIVITÀ_ORA> getAttivitaUtentePeriodo(int idUtente, DateTime frm, DateTime to)
        {
            //prende tutte le attività di un periodo, appartenenti a un certo utente
            var attPeriodo = from a in db.ATTIVITÀ_ORAs
            where (a.giorno.Year > frm.Year ? 1 : a.giorno.Year < frm.Year ? -1 : a.giorno.Month > frm.Month ? 1 : a.giorno.Month < frm.Month ? -1 : a.giorno.Day > frm.Day ? 1 : a.giorno.Day < frm.Day ? -1 : 0) >= 0 && (a.giorno.Year > to.Year ? 1 : a.giorno.Year < to.Year ? -1 : a.giorno.Month > to.Month ? 1 : a.giorno.Month < to.Month ? -1 : a.giorno.Day > to.Day ? 1 : a.giorno.Day < to.Day ? -1 : 0) <= 0
            join au in db.U_ATTIVITAs on a.attività equals au.attività
            where au.utente == idUtente
            select a;
            return attPeriodo;
        }

        public static IQueryable<OBIETTIVO> GetObiettiviUtente(int idUtente)
        {
            return from oc in db.OBBIETTIVO_COMPLETAMENTOs
                   join o in db.OBIETTIVOs on oc.obbiettivo equals o.id
                   where oc.utente == idUtente || oc.utente == ID_ADMIN
                   select o;
        }

        public static OBIETTIVO GetObiettivoUtentePerNome(int idUtente, string descrizioneObiettivo)
        {
            return GetObiettiviUtente(idUtente).Where(o => o.descrizione == descrizioneObiettivo).FirstOrDefault();
        }

        public static int GetNVolteAttivitaPerObiettivoUtente(int idUtente, string nomeAttivita, string descObiettivo)
        {
            return (from s in db.SVOLGIMENTOs
                    join o in db.OBIETTIVOs on s.obbiettivo equals o.id
                    join a in db.ATTIVITÀs on s.attività equals a.id
                    join oc in db.OBBIETTIVO_COMPLETAMENTOs on o.id equals oc.obbiettivo
                    join ac in db.ATTIVITÀ_ORAs on a.id equals ac.attività
                    where ac.utente == idUtente && a.nome == nomeAttivita && o.descrizione == descObiettivo
                    select s).First().numero_da_svolgere;
        }

        public static IQueryable<ACHIEVEMENT> GetAchievement()
        {
            return from a in db.ACHIEVEMENTs
                   select a;
        }

        public static IQueryable<ACHIEVEMNT_COMPLETAMENTO> GetAchievementCompletamentoUtente(int idUtente, int idAc)
        {
            return from ac in db.ACHIEVEMNT_COMPLETAMENTOs
                   join a in db.ACHIEVEMENTs on ac.achievement equals a.id
                   where ac.utente == idUtente && a.id == idAc
                   select ac;
        }

        private static int getNextIdUtente()
        {
            List<UTENTE> utSorted = db.UTENTEs.OrderBy(u => u.id).ToList();
            for (int i = 1; i <= utSorted.Count(); i++)
            {
                if (utSorted.ElementAt(i - 1).id != i)
                    return i;
            }
            return utSorted.Count() + 1;
        }

        private static IQueryable<COMPITO> GetCompitiPerAchievement(ACHIEVEMENT a)
        {
            return from r in db.RICHIEDEs
                   join c in db.COMPITOs on r.compito equals c.id
                   where r.achievement == a.id
                   select c;
        }

       //TODO test
        public static void CambiaSelezioneCompletamentoAttivita(ATTIVITÀ_ORA attOra)
        {
            //ATTIVITÀ att = GetAttivitaUtentePerNome(idUtente, nomeAttivita);
            //var attOra = getAttivitaUtentePeriodo(idUtente, giorno, giorno).Where(a => a.id == id).First();
            int completato = ((bool)attOra.completamento) ? 0 : 1;
            string query = "update ATTIVITÀ_ORA set completamento = {3} where utente = {0} and giorno = {1} and id = {2};";
            db.ExecuteCommand(query, attOra.utente.ToString(), attOra.giorno.ToString(), attOra.id.ToString(), completato.ToString());
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.GIORNATA_CALENDARIOs);
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.ATTIVITÀ_ORAs);
            db = new AgendaDataClassesDataContext();

            //percentuale obbiettivi
            var obbDaInserire = new List<OBBIETTIVO_COMPLETAMENTO>();
            foreach (OBBIETTIVO_COMPLETAMENTO obb in db.OBBIETTIVO_COMPLETAMENTOs.Where(o => o.utente == attOra.utente))
            {
                int completate = 0;
                int totale = 0;
                foreach (SVOLGIMENTO sv in db.SVOLGIMENTOs.Where(s => s.obbiettivo == obb.obbiettivo))
                {
                    int id = Decimal.ToInt32(sv.attività);
                    totale += sv.numero_da_svolgere;
                    completate += getAttivitaUtentePeriodo(Decimal.ToInt32(attOra.utente), obb.data_inizio, obb.data_fine).Where(ao => ao.attività == ao.attività && ((bool)ao.completamento)).Count();
                }
                obb.percentuale_completamento = (completate * 100) / totale;
                obbDaInserire.Add(obb);
                db.OBBIETTIVO_COMPLETAMENTOs.DeleteOnSubmit(obb);
            }
            db.SubmitChanges();
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.GIORNATA_CALENDARIOs);
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.ATTIVITÀ_ORAs);
            db = new AgendaDataClassesDataContext();
            db.OBBIETTIVO_COMPLETAMENTOs.InsertAllOnSubmit(obbDaInserire.ToArray());
            db.SubmitChanges();
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.GIORNATA_CALENDARIOs);
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.ATTIVITÀ_ORAs);
            db = new AgendaDataClassesDataContext();

            //se è una spesa creo un uovo saldo per ogni costo
            foreach (COSTO cost in db.COSTOs.Where(c=> c.attività == attOra.attività))
            {
                SALDO saldoPrec = db.SALDOs.Where(s => s.utente == attOra.utente && s.valuta == cost.valuta).OrderByDescending(s => s.data_ora).First();
                db.SALDOs.InsertOnSubmit(new SALDO
                {
                    utente = attOra.utente,
                    valuta = cost.valuta,
                    data_ora = DateTime.Now,
                    quantità =  saldoPrec.quantità - cost.quantità,
                    SPE_attività = cost.attività,
                    SPE_valuta = cost.valuta
                });
            }
            db.SubmitChanges();
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.GIORNATA_CALENDARIOs);
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.ATTIVITÀ_ORAs);
            db = new AgendaDataClassesDataContext();

            foreach (PROFITTO prof in db.PROFITTOs.Where(c => c.attività == attOra.attività))
            {
                SALDO saldoPrec = db.SALDOs.Where(s => s.utente == attOra.utente && s.valuta == prof.valuta).OrderByDescending(s => s.data_ora).First();
                db.SALDOs.InsertOnSubmit(new SALDO
                {
                    utente = attOra.utente,
                    valuta = prof.valuta,
                    data_ora = DateTime.Now,
                    quantità = saldoPrec.quantità + prof.quantità,
                    ENTR_attività = prof.attività,
                    ENTR_valuta = prof.valuta
                });
            }
            db.SubmitChanges();
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.GIORNATA_CALENDARIOs);
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.ATTIVITÀ_ORAs);
            db = new AgendaDataClassesDataContext();

            //percentuale achievement
            var achDaInserire = new List<ACHIEVEMNT_COMPLETAMENTO>();
            var ocompDaInserire = new List<COMPITO_PERCENTUALE>();
            foreach (ACHIEVEMNT_COMPLETAMENTO aCC in db.ACHIEVEMNT_COMPLETAMENTOs.Where(a => a.utente == attOra.utente))
            {
                int nCompiti = 0;
                int percentuale = 0;
                foreach (COMPITO_PERCENTUALE cP in db.COMPITO_PERCENTUALEs.Where(c => aCC.utente == aCC.utente && c.achievement == aCC.achievement))
                {
                    int completate = 0;
                    COMPITO c = db.COMPITOs.Where(t => t.id == cP.compito).First();
                    int totale = Decimal.ToInt32(c.numero_da_raggiungere);

                    if (!(c.obbiettivo is null))//completamento bbiettivo
                    {
                        completate += db.OBBIETTIVO_COMPLETAMENTOs.Where(o => o.utente == attOra.utente && o.obbiettivo == c.obbiettivo && o.percentuale_completamento == 100).Count();
                    } else if (!(c.attività is null)) //completamento attività
                    {
                        completate += db.ATTIVITÀ_ORAs.Where(a => a.utente == attOra.utente && a.attività == c.attività && ((bool)a.completamento)).Count();
                    } else //guadagno
                    {
                        completate = Decimal.ToInt32((from prof in db.PROFITTOs
                        join ao in db.ATTIVITÀ_ORAs.Where(a => a.utente == attOra.utente && a.attività == c.attività && ((bool)a.completamento)) on prof.attività equals ao.attività
                        where prof.valuta == c.valuta
                        select prof.quantità).Sum());
                    }
                    cP.percentuale_completamento = (completate * 100) / totale;
                    percentuale += Decimal.ToInt32(cP.percentuale_completamento);
                    nCompiti++;
                    ocompDaInserire.Add(cP);
                    db.COMPITO_PERCENTUALEs.DeleteOnSubmit(cP);
                    db.SubmitChanges();
                    db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.COMPITO_PERCENTUALEs);
                    db = new AgendaDataClassesDataContext();
                }
                aCC.completamento_percentuale = percentuale / nCompiti;
                achDaInserire.Add(aCC);
                db.ACHIEVEMNT_COMPLETAMENTOs.DeleteOnSubmit(aCC);
                db.SubmitChanges();
                db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.ACHIEVEMNT_COMPLETAMENTOs);
                db = new AgendaDataClassesDataContext();
            }
            db.ACHIEVEMNT_COMPLETAMENTOs.InsertAllOnSubmit(achDaInserire.ToArray());
            db.SubmitChanges();
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.ACHIEVEMNT_COMPLETAMENTOs);
            db = new AgendaDataClassesDataContext();
            db.COMPITO_PERCENTUALEs.InsertAllOnSubmit(ocompDaInserire.ToArray());
            db.SubmitChanges();
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.COMPITO_PERCENTUALEs);
            db = new AgendaDataClassesDataContext();

        }

        //TODO test
        public static void AggiungiUtente(string nome)
        {
            int id = getNextIdUtente();
            string query = "begin tran addUtente;" +
                "alter table UTENTE nocheck constraint IDUTENTE_CHK;";
            query += "insert into UTENTE(id, nome) values({0},{4});";
            query += "insert into CONTO(utente, valuta) values({0},{1});";
            query += "insert into CONTO(utente, valuta) values({0},{2});";
            query += "insert into CONTO(utente, valuta) values({0},{3});";
            query += "alter table UTENTE with check check constraint ALL;" +
            "commit tran addUtente;";
            db.ExecuteCommand(query, id.ToString(), VALUTA_EURO, VALUTA_PP, VALUTA_PV, nome);
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.CONTOs);
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.UTENTEs);
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.VISIONEs);
            db = new AgendaDataClassesDataContext();
            foreach (ACHIEVEMENT k in GetAchievement())
            {
                var ac = new ACHIEVEMNT_COMPLETAMENTO
                {
                    achievement = k.id,
                    utente = id,
                    completamento_percentuale = 0,
                };
                db.ACHIEVEMNT_COMPLETAMENTOs.InsertOnSubmit(ac);
            }
            db.SubmitChanges();
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.ACHIEVEMNT_COMPLETAMENTOs);
            db = new AgendaDataClassesDataContext();

            foreach (RICHIEDE r in db.RICHIEDEs)
            {
                var nc = new COMPITO_PERCENTUALE
                {
                    utente = id,
                    achievement = r.achievement,
                    compito = r.compito,
                    percentuale_completamento = 0
                };
                db.COMPITO_PERCENTUALEs.InsertOnSubmit(nc);
            }
            db.SubmitChanges();
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.COMPITO_PERCENTUALEs);
            db = new AgendaDataClassesDataContext();

            /*db.VISIONEs.InsertOnSubmit(new VISIONE
            {
                visionato = ID_ADMIN,
                visore = id,
            });
            db.SubmitChanges();
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.VISIONEs);
            db = new AgendaDataClassesDataContext();*/
        }

        //viene chiamata ogni volta che viene aggiunto un utente o che l'admin aggiunge un'attività
        public static void AggiungiAttivitaAdminATutti()
        {
            foreach (ATTIVITÀ att in db.ATTIVITÀs.Where(a => a.id == ID_ADMIN))
            {
                foreach (UTENTE u in db.UTENTEs)
                {
                    if (u.id == ID_ADMIN)
                        continue;
                    var uAtt = new U_ATTIVITA { utente = u.id, attività = att.id };
                    if (!db.U_ATTIVITAs.Contains(uAtt)) //se l'utente u non ha l'attività att dell'admin, gli viene aggiunta
                    {
                        db.U_ATTIVITAs.InsertOnSubmit(uAtt);
                    }
                }
            }
            db.SubmitChanges();
            db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.U_ATTIVITAs);
            db = new AgendaDataClassesDataContext();
        }

        /*public static int compareDateIgnoringTime(DateTime d1, DateTime d2)
        {
            return d1.Year > d2.Year ? 1 : d1.Year < d2.Year ? -1 : d1.Month > d2.Month ? 1 : d1.Month < d2.Month ? -1 : d1.Day > d2.Day ? 1 : d1.Day < d2.Day ? -1 : 0;
        }*/

        public static Func<DateTime, DateTime, int> compareDateIgnoringTime = (d1,d2) => d1.Year > d2.Year ? 1 : d1.Year < d2.Year ? -1 : d1.Month > d2.Month ? 1 : d1.Month < d2.Month ? -1 : d1.Day > d2.Day ? 1 : d1.Day < d2.Day ? -1 : 0;

    }
}
