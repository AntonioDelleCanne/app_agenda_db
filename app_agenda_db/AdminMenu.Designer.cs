﻿namespace app_agenda_db
{
    partial class AdminMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.attStartPanel = new System.Windows.Forms.Panel();
            this.SchermInizAttività = new System.Windows.Forms.SplitContainer();
            this.listaEGruppiSplitContainer = new System.Windows.Forms.SplitContainer();
            this.comboBoxListaAttivita = new System.Windows.Forms.ComboBox();
            this.gruppiAttivitaTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.nomeGroupBox = new System.Windows.Forms.GroupBox();
            this.nomeTextBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.comboBoxColore = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.LabelComboCategoria = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.comboBoxCategoria = new System.Windows.Forms.ComboBox();
            this.button13 = new System.Windows.Forms.Button();
            this.descrizioneGroupBox = new System.Windows.Forms.GroupBox();
            this.descrizioneTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button12 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.b_obiettivo = new System.Windows.Forms.Button();
            this.b_attivita = new System.Windows.Forms.Button();
            this.pannelloAchievement = new System.Windows.Forms.Panel();
            this.obiettivo_split_vista = new System.Windows.Forms.SplitContainer();
            this.obiettivoComboEGroupSplitContainer = new System.Windows.Forms.SplitContainer();
            this.obiettiviLista = new System.Windows.Forms.ComboBox();
            this.obiettivoTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.obiettivoDescrizioneGroupBox = new System.Windows.Forms.GroupBox();
            this.obiettivoDescrizioneTextBox = new System.Windows.Forms.TextBox();
            this.obiettivoSceltaSottoObiettiviGroupBox = new System.Windows.Forms.GroupBox();
            this.obiettivoAggiungiSottoObiettivoButton = new System.Windows.Forms.Button();
            this.obiettivoSceltaSottoObiettivoComboBox = new System.Windows.Forms.ComboBox();
            this.obiettivoDurataGiorniGroupBox = new System.Windows.Forms.GroupBox();
            this.obiettivoAvviaButton = new System.Windows.Forms.Button();
            this.obiettivoALabel = new System.Windows.Forms.Label();
            this.obiettivoDaLabel = new System.Windows.Forms.Label();
            this.obiettivoTermineDurataDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.obiettivoInizioDurataDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.obiettivoSceltaAttivitaGroupBox = new System.Windows.Forms.GroupBox();
            this.obiettivoAggiungiAttivitaButton = new System.Windows.Forms.Button();
            this.obiettivoNVolteTextBox = new System.Windows.Forms.TextBox();
            this.obiettivoNVolteLabel = new System.Windows.Forms.Label();
            this.obiettivoSceltaAttivitaComboBox = new System.Windows.Forms.ComboBox();
            this.obiettivoBottoniTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.obiettivoSalvaButton = new System.Windows.Forms.Button();
            this.obiettivoEliminaButton = new System.Windows.Forms.Button();
            this.obiettivoAggiungiButton = new System.Windows.Forms.Button();
            this.obiettivoVediButton = new System.Windows.Forms.Button();
            this.attStartPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SchermInizAttività)).BeginInit();
            this.SchermInizAttività.Panel1.SuspendLayout();
            this.SchermInizAttività.Panel2.SuspendLayout();
            this.SchermInizAttività.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listaEGruppiSplitContainer)).BeginInit();
            this.listaEGruppiSplitContainer.Panel1.SuspendLayout();
            this.listaEGruppiSplitContainer.Panel2.SuspendLayout();
            this.listaEGruppiSplitContainer.SuspendLayout();
            this.gruppiAttivitaTableLayoutPanel.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.nomeGroupBox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.descrizioneGroupBox.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.pannelloAchievement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.obiettivo_split_vista)).BeginInit();
            this.obiettivo_split_vista.Panel1.SuspendLayout();
            this.obiettivo_split_vista.Panel2.SuspendLayout();
            this.obiettivo_split_vista.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.obiettivoComboEGroupSplitContainer)).BeginInit();
            this.obiettivoComboEGroupSplitContainer.Panel1.SuspendLayout();
            this.obiettivoComboEGroupSplitContainer.Panel2.SuspendLayout();
            this.obiettivoComboEGroupSplitContainer.SuspendLayout();
            this.obiettivoTableLayoutPanel.SuspendLayout();
            this.obiettivoDescrizioneGroupBox.SuspendLayout();
            this.obiettivoSceltaSottoObiettiviGroupBox.SuspendLayout();
            this.obiettivoDurataGiorniGroupBox.SuspendLayout();
            this.obiettivoSceltaAttivitaGroupBox.SuspendLayout();
            this.obiettivoBottoniTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // attStartPanel
            // 
            this.attStartPanel.Controls.Add(this.SchermInizAttività);
            this.attStartPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attStartPanel.Location = new System.Drawing.Point(0, 0);
            this.attStartPanel.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attStartPanel.Name = "attStartPanel";
            this.attStartPanel.Size = new System.Drawing.Size(1134, 748);
            this.attStartPanel.TabIndex = 2;
            // 
            // SchermInizAttività
            // 
            this.SchermInizAttività.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SchermInizAttività.Location = new System.Drawing.Point(0, 0);
            this.SchermInizAttività.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SchermInizAttività.Name = "SchermInizAttività";
            this.SchermInizAttività.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SchermInizAttività.Panel1
            // 
            this.SchermInizAttività.Panel1.Controls.Add(this.listaEGruppiSplitContainer);
            this.SchermInizAttività.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // SchermInizAttività.Panel2
            // 
            this.SchermInizAttività.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.SchermInizAttività.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SchermInizAttività.Size = new System.Drawing.Size(1134, 748);
            this.SchermInizAttività.SplitterDistance = 648;
            this.SchermInizAttività.SplitterWidth = 8;
            this.SchermInizAttività.TabIndex = 1;
            // 
            // listaEGruppiSplitContainer
            // 
            this.listaEGruppiSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listaEGruppiSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.listaEGruppiSplitContainer.Margin = new System.Windows.Forms.Padding(0);
            this.listaEGruppiSplitContainer.Name = "listaEGruppiSplitContainer";
            this.listaEGruppiSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // listaEGruppiSplitContainer.Panel1
            // 
            this.listaEGruppiSplitContainer.Panel1.Controls.Add(this.comboBoxListaAttivita);
            // 
            // listaEGruppiSplitContainer.Panel2
            // 
            this.listaEGruppiSplitContainer.Panel2.Controls.Add(this.gruppiAttivitaTableLayoutPanel);
            this.listaEGruppiSplitContainer.Size = new System.Drawing.Size(1134, 648);
            this.listaEGruppiSplitContainer.SplitterDistance = 48;
            this.listaEGruppiSplitContainer.SplitterWidth = 8;
            this.listaEGruppiSplitContainer.TabIndex = 17;
            // 
            // comboBoxListaAttivita
            // 
            this.comboBoxListaAttivita.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxListaAttivita.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxListaAttivita.FormattingEnabled = true;
            this.comboBoxListaAttivita.Location = new System.Drawing.Point(0, 0);
            this.comboBoxListaAttivita.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxListaAttivita.Name = "comboBoxListaAttivita";
            this.comboBoxListaAttivita.Size = new System.Drawing.Size(1134, 33);
            this.comboBoxListaAttivita.TabIndex = 0;
            this.comboBoxListaAttivita.SelectedIndexChanged += new System.EventHandler(this.ComboBoxListaAttivita_SelectedIndexChanged);
            // 
            // gruppiAttivitaTableLayoutPanel
            // 
            this.gruppiAttivitaTableLayoutPanel.AutoSize = true;
            this.gruppiAttivitaTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gruppiAttivitaTableLayoutPanel.ColumnCount = 3;
            this.gruppiAttivitaTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.gruppiAttivitaTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.gruppiAttivitaTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.gruppiAttivitaTableLayoutPanel.Controls.Add(this.groupBox7, 0, 1);
            this.gruppiAttivitaTableLayoutPanel.Controls.Add(this.nomeGroupBox, 0, 0);
            this.gruppiAttivitaTableLayoutPanel.Controls.Add(this.groupBox3, 2, 1);
            this.gruppiAttivitaTableLayoutPanel.Controls.Add(this.groupBox5, 2, 0);
            this.gruppiAttivitaTableLayoutPanel.Controls.Add(this.descrizioneGroupBox, 1, 0);
            this.gruppiAttivitaTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gruppiAttivitaTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.gruppiAttivitaTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.gruppiAttivitaTableLayoutPanel.Name = "gruppiAttivitaTableLayoutPanel";
            this.gruppiAttivitaTableLayoutPanel.RowCount = 2;
            this.gruppiAttivitaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.gruppiAttivitaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.gruppiAttivitaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.gruppiAttivitaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.gruppiAttivitaTableLayoutPanel.Size = new System.Drawing.Size(1134, 592);
            this.gruppiAttivitaTableLayoutPanel.TabIndex = 16;
            // 
            // groupBox7
            // 
            this.groupBox7.AutoSize = true;
            this.groupBox7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.textBox12);
            this.groupBox7.Controls.Add(this.textBox11);
            this.groupBox7.Controls.Add(this.label9);
            this.groupBox7.Controls.Add(this.textBox10);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Controls.Add(this.label2);
            this.groupBox7.Controls.Add(this.textBox5);
            this.groupBox7.Controls.Add(this.label1);
            this.groupBox7.Controls.Add(this.textBox4);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Location = new System.Drawing.Point(6, 302);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox7.Size = new System.Drawing.Size(365, 284);
            this.groupBox7.TabIndex = 15;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Costo e Premio";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(188, 183);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(175, 25);
            this.label11.TabIndex = 15;
            this.label11.Text = "Punti Produttività";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(194, 213);
            this.textBox12.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(148, 31);
            this.textBox12.TabIndex = 14;
            this.textBox12.TextChanged += new System.EventHandler(this.TextBox12_TextChanged);
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(194, 62);
            this.textBox11.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(148, 31);
            this.textBox11.TabIndex = 10;
            this.textBox11.TextChanged += new System.EventHandler(this.TextBox11_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(188, 108);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(140, 25);
            this.label9.TabIndex = 13;
            this.label9.Text = "Punti Volontà";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(194, 138);
            this.textBox10.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(148, 31);
            this.textBox10.TabIndex = 12;
            this.textBox10.TextChanged += new System.EventHandler(this.TextBox10_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(188, 31);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 25);
            this.label10.TabIndex = 11;
            this.label10.Text = "Euro";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 160);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 25);
            this.label2.TabIndex = 9;
            this.label2.Text = "Punti Volontà";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(18, 217);
            this.textBox5.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(148, 31);
            this.textBox5.TabIndex = 8;
            this.textBox5.TextChanged += new System.EventHandler(this.TextBox5_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 25);
            this.label1.TabIndex = 7;
            this.label1.Text = "Euro";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(18, 94);
            this.textBox4.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(148, 31);
            this.textBox4.TabIndex = 6;
            this.textBox4.TextChanged += new System.EventHandler(this.TextBox4_TextChanged);
            // 
            // nomeGroupBox
            // 
            this.nomeGroupBox.AutoSize = true;
            this.nomeGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.nomeGroupBox.Controls.Add(this.nomeTextBox);
            this.nomeGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nomeGroupBox.Location = new System.Drawing.Point(6, 6);
            this.nomeGroupBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.nomeGroupBox.Name = "nomeGroupBox";
            this.nomeGroupBox.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.nomeGroupBox.Size = new System.Drawing.Size(365, 284);
            this.nomeGroupBox.TabIndex = 9;
            this.nomeGroupBox.TabStop = false;
            this.nomeGroupBox.Text = "Nome";
            // 
            // nomeTextBox
            // 
            this.nomeTextBox.Location = new System.Drawing.Point(46, 108);
            this.nomeTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.nomeTextBox.Name = "nomeTextBox";
            this.nomeTextBox.Size = new System.Drawing.Size(242, 31);
            this.nomeTextBox.TabIndex = 6;
            this.nomeTextBox.TextChanged += new System.EventHandler(this.NomeTextBox_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.AutoSize = true;
            this.groupBox3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox3.Controls.Add(this.comboBoxColore);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(761, 302);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox3.Size = new System.Drawing.Size(367, 284);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Colore";
            // 
            // comboBoxColore
            // 
            this.comboBoxColore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxColore.FormattingEnabled = true;
            this.comboBoxColore.Items.AddRange(new object[] {
            "blu",
            "rosso"});
            this.comboBoxColore.Location = new System.Drawing.Point(122, 127);
            this.comboBoxColore.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.comboBoxColore.Name = "comboBoxColore";
            this.comboBoxColore.Size = new System.Drawing.Size(164, 33);
            this.comboBoxColore.TabIndex = 0;
            this.comboBoxColore.SelectedIndexChanged += new System.EventHandler(this.ComboBoxColore_SelectedIndexChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.AutoSize = true;
            this.groupBox5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.LabelComboCategoria);
            this.groupBox5.Controls.Add(this.textBox6);
            this.groupBox5.Controls.Add(this.comboBoxCategoria);
            this.groupBox5.Controls.Add(this.button13);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(761, 6);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox5.Size = new System.Drawing.Size(367, 284);
            this.groupBox5.TabIndex = 12;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Categoria";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(116, 108);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 25);
            this.label3.TabIndex = 12;
            this.label3.Text = "Nuova";
            // 
            // LabelComboCategoria
            // 
            this.LabelComboCategoria.AutoSize = true;
            this.LabelComboCategoria.Location = new System.Drawing.Point(116, 31);
            this.LabelComboCategoria.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LabelComboCategoria.Name = "LabelComboCategoria";
            this.LabelComboCategoria.Size = new System.Drawing.Size(58, 25);
            this.LabelComboCategoria.TabIndex = 11;
            this.LabelComboCategoria.Text = "Lista";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(122, 138);
            this.textBox6.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(164, 31);
            this.textBox6.TabIndex = 10;
            this.textBox6.TextChanged += new System.EventHandler(this.TextBox6_TextChanged);
            // 
            // comboBoxCategoria
            // 
            this.comboBoxCategoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCategoria.FormattingEnabled = true;
            this.comboBoxCategoria.Location = new System.Drawing.Point(122, 62);
            this.comboBoxCategoria.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.comboBoxCategoria.Name = "comboBoxCategoria";
            this.comboBoxCategoria.Size = new System.Drawing.Size(164, 33);
            this.comboBoxCategoria.TabIndex = 9;
            this.comboBoxCategoria.SelectedIndexChanged += new System.EventHandler(this.ComboBoxCategoria_SelectedIndexChanged);
            // 
            // button13
            // 
            this.button13.Enabled = false;
            this.button13.Location = new System.Drawing.Point(122, 188);
            this.button13.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(168, 56);
            this.button13.TabIndex = 8;
            this.button13.Text = "Aggiungi";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.Button13_Click);
            // 
            // descrizioneGroupBox
            // 
            this.descrizioneGroupBox.AutoSize = true;
            this.descrizioneGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.descrizioneGroupBox.Controls.Add(this.descrizioneTextBox);
            this.descrizioneGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.descrizioneGroupBox.Location = new System.Drawing.Point(383, 6);
            this.descrizioneGroupBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.descrizioneGroupBox.Name = "descrizioneGroupBox";
            this.descrizioneGroupBox.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.descrizioneGroupBox.Size = new System.Drawing.Size(366, 284);
            this.descrizioneGroupBox.TabIndex = 8;
            this.descrizioneGroupBox.TabStop = false;
            this.descrizioneGroupBox.Text = "Descrizione";
            // 
            // descrizioneTextBox
            // 
            this.descrizioneTextBox.Location = new System.Drawing.Point(12, 37);
            this.descrizioneTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.descrizioneTextBox.Multiline = true;
            this.descrizioneTextBox.Name = "descrizioneTextBox";
            this.descrizioneTextBox.Size = new System.Drawing.Size(330, 208);
            this.descrizioneTextBox.TabIndex = 6;
            this.descrizioneTextBox.TextChanged += new System.EventHandler(this.DescrizioneTextBox_TextChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.button12, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button7, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button8, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1134, 92);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // button12
            // 
            this.button12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button12.Enabled = false;
            this.button12.Location = new System.Drawing.Point(0, 0);
            this.button12.Margin = new System.Windows.Forms.Padding(0);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(378, 92);
            this.button12.TabIndex = 4;
            this.button12.Text = "Elimina Attività";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.Button12_Click);
            // 
            // button7
            // 
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.Enabled = false;
            this.button7.Location = new System.Drawing.Point(378, 0);
            this.button7.Margin = new System.Windows.Forms.Padding(0);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(378, 92);
            this.button7.TabIndex = 3;
            this.button7.Text = "Salva Attività";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Button7_Click);
            // 
            // button8
            // 
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.Enabled = false;
            this.button8.Location = new System.Drawing.Point(756, 0);
            this.button8.Margin = new System.Windows.Forms.Padding(0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(378, 92);
            this.button8.TabIndex = 2;
            this.button8.Text = "Aggiungi Attività";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Button8_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.b_obiettivo);
            this.splitContainer1.Panel1.Controls.Add(this.b_attivita);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.pannelloAchievement);
            this.splitContainer1.Panel2.Controls.Add(this.attStartPanel);
            this.splitContainer1.Size = new System.Drawing.Size(1370, 748);
            this.splitContainer1.SplitterDistance = 228;
            this.splitContainer1.SplitterWidth = 8;
            this.splitContainer1.TabIndex = 3;
            // 
            // b_obiettivo
            // 
            this.b_obiettivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_obiettivo.Location = new System.Drawing.Point(0, 90);
            this.b_obiettivo.Margin = new System.Windows.Forms.Padding(0);
            this.b_obiettivo.Name = "b_obiettivo";
            this.b_obiettivo.Size = new System.Drawing.Size(230, 110);
            this.b_obiettivo.TabIndex = 1;
            this.b_obiettivo.Text = "Obiettivi";
            this.b_obiettivo.UseVisualStyleBackColor = true;
            this.b_obiettivo.Click += new System.EventHandler(this.B_obiettivo_Click);
            // 
            // b_attivita
            // 
            this.b_attivita.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_attivita.Location = new System.Drawing.Point(0, 0);
            this.b_attivita.Margin = new System.Windows.Forms.Padding(0);
            this.b_attivita.Name = "b_attivita";
            this.b_attivita.Size = new System.Drawing.Size(230, 90);
            this.b_attivita.TabIndex = 0;
            this.b_attivita.Text = "Attività";
            this.b_attivita.UseVisualStyleBackColor = true;
            this.b_attivita.Click += new System.EventHandler(this.B_attivita_Click);
            // 
            // pannelloAchievement
            // 
            this.pannelloAchievement.AutoSize = true;
            this.pannelloAchievement.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pannelloAchievement.Controls.Add(this.obiettivo_split_vista);
            this.pannelloAchievement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pannelloAchievement.Location = new System.Drawing.Point(0, 0);
            this.pannelloAchievement.Margin = new System.Windows.Forms.Padding(0);
            this.pannelloAchievement.Name = "pannelloAchievement";
            this.pannelloAchievement.Size = new System.Drawing.Size(1134, 748);
            this.pannelloAchievement.TabIndex = 3;
            // 
            // obiettivo_split_vista
            // 
            this.obiettivo_split_vista.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivo_split_vista.Location = new System.Drawing.Point(0, 0);
            this.obiettivo_split_vista.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivo_split_vista.Name = "obiettivo_split_vista";
            this.obiettivo_split_vista.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // obiettivo_split_vista.Panel1
            // 
            this.obiettivo_split_vista.Panel1.Controls.Add(this.obiettivoComboEGroupSplitContainer);
            // 
            // obiettivo_split_vista.Panel2
            // 
            this.obiettivo_split_vista.Panel2.Controls.Add(this.obiettivoBottoniTableLayoutPanel);
            this.obiettivo_split_vista.Size = new System.Drawing.Size(1134, 748);
            this.obiettivo_split_vista.SplitterDistance = 630;
            this.obiettivo_split_vista.TabIndex = 1;
            // 
            // obiettivoComboEGroupSplitContainer
            // 
            this.obiettivoComboEGroupSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoComboEGroupSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.obiettivoComboEGroupSplitContainer.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.obiettivoComboEGroupSplitContainer.Name = "obiettivoComboEGroupSplitContainer";
            this.obiettivoComboEGroupSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // obiettivoComboEGroupSplitContainer.Panel1
            // 
            this.obiettivoComboEGroupSplitContainer.Panel1.Controls.Add(this.obiettiviLista);
            // 
            // obiettivoComboEGroupSplitContainer.Panel2
            // 
            this.obiettivoComboEGroupSplitContainer.Panel2.Controls.Add(this.obiettivoTableLayoutPanel);
            this.obiettivoComboEGroupSplitContainer.Size = new System.Drawing.Size(1134, 630);
            this.obiettivoComboEGroupSplitContainer.SplitterDistance = 48;
            this.obiettivoComboEGroupSplitContainer.SplitterWidth = 8;
            this.obiettivoComboEGroupSplitContainer.TabIndex = 7;
            // 
            // obiettiviLista
            // 
            this.obiettiviLista.Dock = System.Windows.Forms.DockStyle.Top;
            this.obiettiviLista.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.obiettiviLista.FormattingEnabled = true;
            this.obiettiviLista.Location = new System.Drawing.Point(0, 0);
            this.obiettiviLista.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettiviLista.Name = "obiettiviLista";
            this.obiettiviLista.Size = new System.Drawing.Size(1134, 33);
            this.obiettiviLista.TabIndex = 0;
            // 
            // obiettivoTableLayoutPanel
            // 
            this.obiettivoTableLayoutPanel.AutoSize = true;
            this.obiettivoTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoTableLayoutPanel.ColumnCount = 2;
            this.obiettivoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.obiettivoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.obiettivoTableLayoutPanel.Controls.Add(this.obiettivoDescrizioneGroupBox, 0, 0);
            this.obiettivoTableLayoutPanel.Controls.Add(this.obiettivoSceltaSottoObiettiviGroupBox, 1, 1);
            this.obiettivoTableLayoutPanel.Controls.Add(this.obiettivoDurataGiorniGroupBox, 1, 0);
            this.obiettivoTableLayoutPanel.Controls.Add(this.obiettivoSceltaAttivitaGroupBox, 0, 1);
            this.obiettivoTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.obiettivoTableLayoutPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoTableLayoutPanel.Name = "obiettivoTableLayoutPanel";
            this.obiettivoTableLayoutPanel.RowCount = 2;
            this.obiettivoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.obiettivoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.obiettivoTableLayoutPanel.Size = new System.Drawing.Size(1134, 574);
            this.obiettivoTableLayoutPanel.TabIndex = 6;
            // 
            // obiettivoDescrizioneGroupBox
            // 
            this.obiettivoDescrizioneGroupBox.AutoSize = true;
            this.obiettivoDescrizioneGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoDescrizioneGroupBox.Controls.Add(this.obiettivoDescrizioneTextBox);
            this.obiettivoDescrizioneGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoDescrizioneGroupBox.Location = new System.Drawing.Point(4, 4);
            this.obiettivoDescrizioneGroupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoDescrizioneGroupBox.Name = "obiettivoDescrizioneGroupBox";
            this.obiettivoDescrizioneGroupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoDescrizioneGroupBox.Size = new System.Drawing.Size(559, 279);
            this.obiettivoDescrizioneGroupBox.TabIndex = 1;
            this.obiettivoDescrizioneGroupBox.TabStop = false;
            this.obiettivoDescrizioneGroupBox.Text = "Descrizione";
            // 
            // obiettivoDescrizioneTextBox
            // 
            this.obiettivoDescrizioneTextBox.Location = new System.Drawing.Point(52, 90);
            this.obiettivoDescrizioneTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoDescrizioneTextBox.Multiline = true;
            this.obiettivoDescrizioneTextBox.Name = "obiettivoDescrizioneTextBox";
            this.obiettivoDescrizioneTextBox.Size = new System.Drawing.Size(396, 141);
            this.obiettivoDescrizioneTextBox.TabIndex = 0;
            this.obiettivoDescrizioneTextBox.TextChanged += new System.EventHandler(this.ObiettivoDescrizioneTextBox_TextChanged);
            // 
            // obiettivoSceltaSottoObiettiviGroupBox
            // 
            this.obiettivoSceltaSottoObiettiviGroupBox.AutoSize = true;
            this.obiettivoSceltaSottoObiettiviGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoSceltaSottoObiettiviGroupBox.Controls.Add(this.obiettivoAggiungiSottoObiettivoButton);
            this.obiettivoSceltaSottoObiettiviGroupBox.Controls.Add(this.obiettivoSceltaSottoObiettivoComboBox);
            this.obiettivoSceltaSottoObiettiviGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoSceltaSottoObiettiviGroupBox.Location = new System.Drawing.Point(571, 291);
            this.obiettivoSceltaSottoObiettiviGroupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoSceltaSottoObiettiviGroupBox.Name = "obiettivoSceltaSottoObiettiviGroupBox";
            this.obiettivoSceltaSottoObiettiviGroupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoSceltaSottoObiettiviGroupBox.Size = new System.Drawing.Size(559, 279);
            this.obiettivoSceltaSottoObiettiviGroupBox.TabIndex = 3;
            this.obiettivoSceltaSottoObiettiviGroupBox.TabStop = false;
            this.obiettivoSceltaSottoObiettiviGroupBox.Text = "Sotto-obiettivo/i da completare";
            // 
            // obiettivoAggiungiSottoObiettivoButton
            // 
            this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
            this.obiettivoAggiungiSottoObiettivoButton.Location = new System.Drawing.Point(186, 171);
            this.obiettivoAggiungiSottoObiettivoButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoAggiungiSottoObiettivoButton.Name = "obiettivoAggiungiSottoObiettivoButton";
            this.obiettivoAggiungiSottoObiettivoButton.Size = new System.Drawing.Size(184, 54);
            this.obiettivoAggiungiSottoObiettivoButton.TabIndex = 1;
            this.obiettivoAggiungiSottoObiettivoButton.Text = "Scegli obiettivo";
            this.obiettivoAggiungiSottoObiettivoButton.UseVisualStyleBackColor = true;
            this.obiettivoAggiungiSottoObiettivoButton.Click += new System.EventHandler(this.ObiettivoAggiungiSottoObiettivoButton_Click);
            // 
            // obiettivoSceltaSottoObiettivoComboBox
            // 
            this.obiettivoSceltaSottoObiettivoComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.obiettivoSceltaSottoObiettivoComboBox.FormattingEnabled = true;
            this.obiettivoSceltaSottoObiettivoComboBox.Items.AddRange(new object[] {
            "a",
            "b"});
            this.obiettivoSceltaSottoObiettivoComboBox.Location = new System.Drawing.Point(186, 98);
            this.obiettivoSceltaSottoObiettivoComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoSceltaSottoObiettivoComboBox.Name = "obiettivoSceltaSottoObiettivoComboBox";
            this.obiettivoSceltaSottoObiettivoComboBox.Size = new System.Drawing.Size(180, 33);
            this.obiettivoSceltaSottoObiettivoComboBox.TabIndex = 0;
            this.obiettivoSceltaSottoObiettivoComboBox.SelectedIndexChanged += new System.EventHandler(this.ObiettivoSceltaSottoObiettivoComboBox_SelectedIndexChanged);
            // 
            // obiettivoDurataGiorniGroupBox
            // 
            this.obiettivoDurataGiorniGroupBox.AutoSize = true;
            this.obiettivoDurataGiorniGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoDurataGiorniGroupBox.Controls.Add(this.obiettivoAvviaButton);
            this.obiettivoDurataGiorniGroupBox.Controls.Add(this.obiettivoALabel);
            this.obiettivoDurataGiorniGroupBox.Controls.Add(this.obiettivoDaLabel);
            this.obiettivoDurataGiorniGroupBox.Controls.Add(this.obiettivoTermineDurataDateTimePicker);
            this.obiettivoDurataGiorniGroupBox.Controls.Add(this.obiettivoInizioDurataDateTimePicker);
            this.obiettivoDurataGiorniGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoDurataGiorniGroupBox.Location = new System.Drawing.Point(571, 4);
            this.obiettivoDurataGiorniGroupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoDurataGiorniGroupBox.Name = "obiettivoDurataGiorniGroupBox";
            this.obiettivoDurataGiorniGroupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoDurataGiorniGroupBox.Size = new System.Drawing.Size(559, 279);
            this.obiettivoDurataGiorniGroupBox.TabIndex = 4;
            this.obiettivoDurataGiorniGroupBox.TabStop = false;
            this.obiettivoDurataGiorniGroupBox.Text = "Durata";
            // 
            // obiettivoAvviaButton
            // 
            this.obiettivoAvviaButton.Enabled = false;
            this.obiettivoAvviaButton.Location = new System.Drawing.Point(176, 208);
            this.obiettivoAvviaButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoAvviaButton.Name = "obiettivoAvviaButton";
            this.obiettivoAvviaButton.Size = new System.Drawing.Size(200, 54);
            this.obiettivoAvviaButton.TabIndex = 4;
            this.obiettivoAvviaButton.Text = "Avvia";
            this.obiettivoAvviaButton.UseVisualStyleBackColor = true;
            this.obiettivoAvviaButton.Click += new System.EventHandler(this.ObiettivoAvviaButton_Click);
            // 
            // obiettivoALabel
            // 
            this.obiettivoALabel.AutoSize = true;
            this.obiettivoALabel.Location = new System.Drawing.Point(170, 115);
            this.obiettivoALabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.obiettivoALabel.Name = "obiettivoALabel";
            this.obiettivoALabel.Size = new System.Drawing.Size(26, 25);
            this.obiettivoALabel.TabIndex = 3;
            this.obiettivoALabel.Text = "A";
            // 
            // obiettivoDaLabel
            // 
            this.obiettivoDaLabel.AutoSize = true;
            this.obiettivoDaLabel.Location = new System.Drawing.Point(170, 40);
            this.obiettivoDaLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.obiettivoDaLabel.Name = "obiettivoDaLabel";
            this.obiettivoDaLabel.Size = new System.Drawing.Size(39, 25);
            this.obiettivoDaLabel.TabIndex = 2;
            this.obiettivoDaLabel.Text = "Da";
            // 
            // obiettivoTermineDurataDateTimePicker
            // 
            this.obiettivoTermineDurataDateTimePicker.Enabled = false;
            this.obiettivoTermineDurataDateTimePicker.Location = new System.Drawing.Point(176, 148);
            this.obiettivoTermineDurataDateTimePicker.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoTermineDurataDateTimePicker.Name = "obiettivoTermineDurataDateTimePicker";
            this.obiettivoTermineDurataDateTimePicker.Size = new System.Drawing.Size(196, 31);
            this.obiettivoTermineDurataDateTimePicker.TabIndex = 1;
            // 
            // obiettivoInizioDurataDateTimePicker
            // 
            this.obiettivoInizioDurataDateTimePicker.Enabled = false;
            this.obiettivoInizioDurataDateTimePicker.Location = new System.Drawing.Point(176, 73);
            this.obiettivoInizioDurataDateTimePicker.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoInizioDurataDateTimePicker.Name = "obiettivoInizioDurataDateTimePicker";
            this.obiettivoInizioDurataDateTimePicker.Size = new System.Drawing.Size(194, 31);
            this.obiettivoInizioDurataDateTimePicker.TabIndex = 0;
            // 
            // obiettivoSceltaAttivitaGroupBox
            // 
            this.obiettivoSceltaAttivitaGroupBox.AutoSize = true;
            this.obiettivoSceltaAttivitaGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoSceltaAttivitaGroupBox.Controls.Add(this.obiettivoAggiungiAttivitaButton);
            this.obiettivoSceltaAttivitaGroupBox.Controls.Add(this.obiettivoNVolteTextBox);
            this.obiettivoSceltaAttivitaGroupBox.Controls.Add(this.obiettivoNVolteLabel);
            this.obiettivoSceltaAttivitaGroupBox.Controls.Add(this.obiettivoSceltaAttivitaComboBox);
            this.obiettivoSceltaAttivitaGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoSceltaAttivitaGroupBox.Location = new System.Drawing.Point(4, 291);
            this.obiettivoSceltaAttivitaGroupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoSceltaAttivitaGroupBox.Name = "obiettivoSceltaAttivitaGroupBox";
            this.obiettivoSceltaAttivitaGroupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoSceltaAttivitaGroupBox.Size = new System.Drawing.Size(559, 279);
            this.obiettivoSceltaAttivitaGroupBox.TabIndex = 5;
            this.obiettivoSceltaAttivitaGroupBox.TabStop = false;
            this.obiettivoSceltaAttivitaGroupBox.Text = "Attività da completare (con quante volte)";
            // 
            // obiettivoAggiungiAttivitaButton
            // 
            this.obiettivoAggiungiAttivitaButton.Enabled = false;
            this.obiettivoAggiungiAttivitaButton.Location = new System.Drawing.Point(164, 187);
            this.obiettivoAggiungiAttivitaButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoAggiungiAttivitaButton.Name = "obiettivoAggiungiAttivitaButton";
            this.obiettivoAggiungiAttivitaButton.Size = new System.Drawing.Size(202, 46);
            this.obiettivoAggiungiAttivitaButton.TabIndex = 3;
            this.obiettivoAggiungiAttivitaButton.Text = "Scegli attività";
            this.obiettivoAggiungiAttivitaButton.UseVisualStyleBackColor = true;
            this.obiettivoAggiungiAttivitaButton.Click += new System.EventHandler(this.ObiettivoAggiungiAttivitaButton_Click);
            // 
            // obiettivoNVolteTextBox
            // 
            this.obiettivoNVolteTextBox.Location = new System.Drawing.Point(164, 133);
            this.obiettivoNVolteTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoNVolteTextBox.Name = "obiettivoNVolteTextBox";
            this.obiettivoNVolteTextBox.Size = new System.Drawing.Size(196, 31);
            this.obiettivoNVolteTextBox.TabIndex = 2;
            this.obiettivoNVolteTextBox.TextChanged += new System.EventHandler(this.ObiettivoNVolteTextBox_TextChanged);
            // 
            // obiettivoNVolteLabel
            // 
            this.obiettivoNVolteLabel.AutoSize = true;
            this.obiettivoNVolteLabel.Location = new System.Drawing.Point(158, 104);
            this.obiettivoNVolteLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.obiettivoNVolteLabel.Name = "obiettivoNVolteLabel";
            this.obiettivoNVolteLabel.Size = new System.Drawing.Size(35, 25);
            this.obiettivoNVolteLabel.TabIndex = 1;
            this.obiettivoNVolteLabel.Text = "N°";
            // 
            // obiettivoSceltaAttivitaComboBox
            // 
            this.obiettivoSceltaAttivitaComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.obiettivoSceltaAttivitaComboBox.FormattingEnabled = true;
            this.obiettivoSceltaAttivitaComboBox.Items.AddRange(new object[] {
            "uno",
            "due"});
            this.obiettivoSceltaAttivitaComboBox.Location = new System.Drawing.Point(164, 58);
            this.obiettivoSceltaAttivitaComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoSceltaAttivitaComboBox.Name = "obiettivoSceltaAttivitaComboBox";
            this.obiettivoSceltaAttivitaComboBox.Size = new System.Drawing.Size(196, 33);
            this.obiettivoSceltaAttivitaComboBox.TabIndex = 0;
            this.obiettivoSceltaAttivitaComboBox.SelectedIndexChanged += new System.EventHandler(this.ObiettivoSceltaAttivitaComboBox_SelectedIndexChanged);
            // 
            // obiettivoBottoniTableLayoutPanel
            // 
            this.obiettivoBottoniTableLayoutPanel.AutoSize = true;
            this.obiettivoBottoniTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoBottoniTableLayoutPanel.ColumnCount = 4;
            this.obiettivoBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.obiettivoBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.obiettivoBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.obiettivoBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.obiettivoBottoniTableLayoutPanel.Controls.Add(this.obiettivoSalvaButton, 1, 0);
            this.obiettivoBottoniTableLayoutPanel.Controls.Add(this.obiettivoEliminaButton, 0, 0);
            this.obiettivoBottoniTableLayoutPanel.Controls.Add(this.obiettivoAggiungiButton, 3, 0);
            this.obiettivoBottoniTableLayoutPanel.Controls.Add(this.obiettivoVediButton, 2, 0);
            this.obiettivoBottoniTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoBottoniTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.obiettivoBottoniTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.obiettivoBottoniTableLayoutPanel.Name = "obiettivoBottoniTableLayoutPanel";
            this.obiettivoBottoniTableLayoutPanel.RowCount = 1;
            this.obiettivoBottoniTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.obiettivoBottoniTableLayoutPanel.Size = new System.Drawing.Size(1134, 114);
            this.obiettivoBottoniTableLayoutPanel.TabIndex = 3;
            // 
            // obiettivoSalvaButton
            // 
            this.obiettivoSalvaButton.AutoSize = true;
            this.obiettivoSalvaButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoSalvaButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoSalvaButton.Enabled = false;
            this.obiettivoSalvaButton.Location = new System.Drawing.Point(283, 0);
            this.obiettivoSalvaButton.Margin = new System.Windows.Forms.Padding(0);
            this.obiettivoSalvaButton.Name = "obiettivoSalvaButton";
            this.obiettivoSalvaButton.Size = new System.Drawing.Size(283, 114);
            this.obiettivoSalvaButton.TabIndex = 2;
            this.obiettivoSalvaButton.Text = "Salva obiettivo";
            this.obiettivoSalvaButton.UseVisualStyleBackColor = true;
            this.obiettivoSalvaButton.Click += new System.EventHandler(this.ObiettivoSalvaButton_Click);
            // 
            // obiettivoEliminaButton
            // 
            this.obiettivoEliminaButton.AutoSize = true;
            this.obiettivoEliminaButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoEliminaButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoEliminaButton.Enabled = false;
            this.obiettivoEliminaButton.Location = new System.Drawing.Point(0, 0);
            this.obiettivoEliminaButton.Margin = new System.Windows.Forms.Padding(0);
            this.obiettivoEliminaButton.Name = "obiettivoEliminaButton";
            this.obiettivoEliminaButton.Size = new System.Drawing.Size(283, 114);
            this.obiettivoEliminaButton.TabIndex = 1;
            this.obiettivoEliminaButton.Text = "Elimina obiettivo";
            this.obiettivoEliminaButton.UseVisualStyleBackColor = true;
            this.obiettivoEliminaButton.Click += new System.EventHandler(this.ObiettivoEliminaButton_Click);
            // 
            // obiettivoAggiungiButton
            // 
            this.obiettivoAggiungiButton.AutoSize = true;
            this.obiettivoAggiungiButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoAggiungiButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoAggiungiButton.Enabled = false;
            this.obiettivoAggiungiButton.Location = new System.Drawing.Point(849, 0);
            this.obiettivoAggiungiButton.Margin = new System.Windows.Forms.Padding(0);
            this.obiettivoAggiungiButton.Name = "obiettivoAggiungiButton";
            this.obiettivoAggiungiButton.Size = new System.Drawing.Size(285, 114);
            this.obiettivoAggiungiButton.TabIndex = 0;
            this.obiettivoAggiungiButton.Text = "Aggiungi obiettivo";
            this.obiettivoAggiungiButton.UseVisualStyleBackColor = true;
            this.obiettivoAggiungiButton.Click += new System.EventHandler(this.ObiettivoAggiungiButton_Click);
            // 
            // obiettivoVediButton
            // 
            this.obiettivoVediButton.AutoSize = true;
            this.obiettivoVediButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoVediButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoVediButton.Location = new System.Drawing.Point(566, 0);
            this.obiettivoVediButton.Margin = new System.Windows.Forms.Padding(0);
            this.obiettivoVediButton.Name = "obiettivoVediButton";
            this.obiettivoVediButton.Size = new System.Drawing.Size(283, 114);
            this.obiettivoVediButton.TabIndex = 3;
            this.obiettivoVediButton.Text = "Vedi obiettivi";
            this.obiettivoVediButton.UseVisualStyleBackColor = true;
            this.obiettivoVediButton.Click += new System.EventHandler(this.ObiettivoVediButton_Click);
            // 
            // AdminMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 748);
            this.Controls.Add(this.splitContainer1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "AdminMenu";
            this.Text = "AdminMenu";
            this.attStartPanel.ResumeLayout(false);
            this.SchermInizAttività.Panel1.ResumeLayout(false);
            this.SchermInizAttività.Panel2.ResumeLayout(false);
            this.SchermInizAttività.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SchermInizAttività)).EndInit();
            this.SchermInizAttività.ResumeLayout(false);
            this.listaEGruppiSplitContainer.Panel1.ResumeLayout(false);
            this.listaEGruppiSplitContainer.Panel2.ResumeLayout(false);
            this.listaEGruppiSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listaEGruppiSplitContainer)).EndInit();
            this.listaEGruppiSplitContainer.ResumeLayout(false);
            this.gruppiAttivitaTableLayoutPanel.ResumeLayout(false);
            this.gruppiAttivitaTableLayoutPanel.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.nomeGroupBox.ResumeLayout(false);
            this.nomeGroupBox.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.descrizioneGroupBox.ResumeLayout(false);
            this.descrizioneGroupBox.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.pannelloAchievement.ResumeLayout(false);
            this.obiettivo_split_vista.Panel1.ResumeLayout(false);
            this.obiettivo_split_vista.Panel2.ResumeLayout(false);
            this.obiettivo_split_vista.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.obiettivo_split_vista)).EndInit();
            this.obiettivo_split_vista.ResumeLayout(false);
            this.obiettivoComboEGroupSplitContainer.Panel1.ResumeLayout(false);
            this.obiettivoComboEGroupSplitContainer.Panel2.ResumeLayout(false);
            this.obiettivoComboEGroupSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.obiettivoComboEGroupSplitContainer)).EndInit();
            this.obiettivoComboEGroupSplitContainer.ResumeLayout(false);
            this.obiettivoTableLayoutPanel.ResumeLayout(false);
            this.obiettivoTableLayoutPanel.PerformLayout();
            this.obiettivoDescrizioneGroupBox.ResumeLayout(false);
            this.obiettivoDescrizioneGroupBox.PerformLayout();
            this.obiettivoSceltaSottoObiettiviGroupBox.ResumeLayout(false);
            this.obiettivoDurataGiorniGroupBox.ResumeLayout(false);
            this.obiettivoDurataGiorniGroupBox.PerformLayout();
            this.obiettivoSceltaAttivitaGroupBox.ResumeLayout(false);
            this.obiettivoSceltaAttivitaGroupBox.PerformLayout();
            this.obiettivoBottoniTableLayoutPanel.ResumeLayout(false);
            this.obiettivoBottoniTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel attStartPanel;
        private System.Windows.Forms.SplitContainer SchermInizAttività;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LabelComboCategoria;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.ComboBox comboBoxCategoria;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox comboBoxColore;
        private System.Windows.Forms.GroupBox nomeGroupBox;
        private System.Windows.Forms.TextBox nomeTextBox;
        private System.Windows.Forms.GroupBox descrizioneGroupBox;
        private System.Windows.Forms.TextBox descrizioneTextBox;
        private System.Windows.Forms.ComboBox comboBoxListaAttivita;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.SplitContainer listaEGruppiSplitContainer;
        private System.Windows.Forms.TableLayoutPanel gruppiAttivitaTableLayoutPanel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button b_obiettivo;
        private System.Windows.Forms.Button b_attivita;
        private System.Windows.Forms.Panel pannelloAchievement;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.SplitContainer obiettivo_split_vista;
        private System.Windows.Forms.SplitContainer obiettivoComboEGroupSplitContainer;
        private System.Windows.Forms.ComboBox obiettiviLista;
        private System.Windows.Forms.TableLayoutPanel obiettivoTableLayoutPanel;
        private System.Windows.Forms.GroupBox obiettivoDescrizioneGroupBox;
        private System.Windows.Forms.TextBox obiettivoDescrizioneTextBox;
        private System.Windows.Forms.GroupBox obiettivoSceltaSottoObiettiviGroupBox;
        private System.Windows.Forms.Button obiettivoAggiungiSottoObiettivoButton;
        private System.Windows.Forms.ComboBox obiettivoSceltaSottoObiettivoComboBox;
        private System.Windows.Forms.GroupBox obiettivoDurataGiorniGroupBox;
        private System.Windows.Forms.Button obiettivoAvviaButton;
        private System.Windows.Forms.Label obiettivoALabel;
        private System.Windows.Forms.Label obiettivoDaLabel;
        private System.Windows.Forms.DateTimePicker obiettivoTermineDurataDateTimePicker;
        private System.Windows.Forms.DateTimePicker obiettivoInizioDurataDateTimePicker;
        private System.Windows.Forms.GroupBox obiettivoSceltaAttivitaGroupBox;
        private System.Windows.Forms.Button obiettivoAggiungiAttivitaButton;
        private System.Windows.Forms.TextBox obiettivoNVolteTextBox;
        private System.Windows.Forms.Label obiettivoNVolteLabel;
        private System.Windows.Forms.ComboBox obiettivoSceltaAttivitaComboBox;
        private System.Windows.Forms.TableLayoutPanel obiettivoBottoniTableLayoutPanel;
        private System.Windows.Forms.Button obiettivoSalvaButton;
        private System.Windows.Forms.Button obiettivoEliminaButton;
        private System.Windows.Forms.Button obiettivoAggiungiButton;
        private System.Windows.Forms.Button obiettivoVediButton;
    }
}